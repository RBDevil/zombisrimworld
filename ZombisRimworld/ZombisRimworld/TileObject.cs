﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace ZombisRimworld
{
    public class TileObject
    {
        protected Texture2D texture;
        protected Tile tile;
        public Tile Tile { get => tile; }
        protected Item drop;
        public Item Drop { get => drop; }
        
        public TileObject(Texture2D texture, Tile tile)
        {
            this.texture = texture;
            this.tile = tile;
        }

        public virtual void Update(Map map)
        {
            
        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, tile.Position, Color.White);
        }
    }
}
