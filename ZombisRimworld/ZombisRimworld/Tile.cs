﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;
using ZombisRimworld.UI;

namespace ZombisRimworld
{
    public class Tile
    {
        bool hovered;
        public bool Hovered { get => hovered; }
        Texture2D texture;
        TileObject tileObject;
        public TileObject TileObject { get => TileObject; }
        Vector2 position;
        public Vector2 Position { get => position; }

        public Tile(Texture2D texture, Vector2 position)
        {
            this.texture = texture;
            this.position = position;
        }

        public void newTileObject(TileObject tileObject)
        {
            this.tileObject = tileObject;
        }

        public Item destructTileObject()
        {
            Item ret = tileObject.Drop;
            tileObject = null;
            return ret;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, position, Color.White);
            if(tileObject != null)
            {
                tileObject.Draw(spriteBatch);
            }
            else if (hovered && MouseEvents.Build)
            {
                spriteBatch.Draw(Game1.textures["hoverTile"], position, Color.White);
            }
        }

        public void Update(Tile hoveredTile, Map map)
        {
            if (hoveredTile == this)
            {
                hovered = true;
            }
            else hovered = false;
            if(tileObject != null)
            {
                tileObject.Update(map);
            }
        }
    }
}
