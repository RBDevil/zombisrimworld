﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace ZombisRimworld.UI
{
    class UserInterface
    {
        Dictionary<string, Button> buttons;
        public Dictionary<string, Button> Buttons { get => buttons; }

        public UserInterface()
        {
            buttons = new Dictionary<string, Button>();
        }

        public void AddUIEntity(string key, Button button)
        {
            buttons.Add(key, button);
        }

        public void Clear()
        {
            buttons.Clear();
        }

        public void Update(bool leftClick, bool leftButtonPressed, Point mousePosition)
        {
            foreach (Button button in buttons.Values)
            {
                button.Update(leftClick, leftButtonPressed, mousePosition);
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            foreach (Button button in buttons.Values)
            {
                button.Draw(spriteBatch);
            }
        }
    }
}
