﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Text;

namespace ZombisRimworld.UI
{
    static class MouseEvents
    {
        static bool leftClick, previousLeftButton, leftButton, selectTree, build;
        public static bool SelectTree { get => selectTree; set => selectTree = value; }
        public static bool Build { get => build; set => build = value; }
        static Point mousePosition;
        public static Point MousePosition { get => mousePosition; }
        public static bool LeftClick { get => leftClick; }
        public static bool LeftButton { get => leftButton; }

        public static void Update()
        {
            previousLeftButton = leftButton;
            MouseState mstate = Mouse.GetState();
            leftButton = mstate.LeftButton == ButtonState.Pressed;
            if (previousLeftButton && !leftButton)
            {
                leftClick = true;
            }
            else leftClick = false;
            mousePosition = mstate.Position;
        }
    }
}
