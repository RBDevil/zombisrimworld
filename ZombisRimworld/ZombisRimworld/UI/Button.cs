﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace ZombisRimworld.UI
{
    class Button
    {
        protected Texture2D currentTexture, defaultTexture, hoveredTexture, clickedTexture;
        protected Vector2 position;
        protected Rectangle rectangle;
        protected bool clicked;
        public bool Clicked { get => clicked; }
        public Button(Texture2D defaultTexture, Vector2 position, Texture2D hoveredTexture, Texture2D clickedTexture)
        {
            currentTexture = defaultTexture;
            this.defaultTexture = defaultTexture;
            this.hoveredTexture = hoveredTexture;
            this.clickedTexture = clickedTexture;
            this.position = position;
            rectangle = new Rectangle((int)position.X, (int)position.Y, defaultTexture.Width, defaultTexture.Height);
            if (hoveredTexture == null) this.hoveredTexture = defaultTexture;
            if (clickedTexture == null) this.clickedTexture = defaultTexture;
        }

        public virtual void OnClick()
        {

        }

        bool IsHovered(Point mousePosition)
        {
            if (mousePosition.X >= position.X &&
                mousePosition.Y >= position.Y &&
                mousePosition.X <= position.X + rectangle.Width &&
                mousePosition.Y <= position.Y + rectangle.Height)
            {
                return true;
            }
            else return false;
        }

        public virtual void Update(bool LeftClick, bool leftButtonPressed, Point mousePosition)
        {
            clicked = false;
            if (IsHovered(mousePosition))
            {
                if (leftButtonPressed)
                {
                    currentTexture = clickedTexture;
                }
                else currentTexture = hoveredTexture;
                if (LeftClick)
                {
                    OnClick();
                    clicked = true;
                }
            }
            else currentTexture = defaultTexture;
        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(currentTexture, position, Color.White);
        }
    }
}
