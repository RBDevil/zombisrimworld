﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace ZombisRimworld
{
    class Human
    {
        Texture2D texture;
        Vector2 position, velocity, direction;
        float speed;
        
        public Human(Texture2D texture, Vector2 position)
        {
            this.texture = texture;
            this.position = position;
            direction = new Vector2(1,0);
        }

        void ChopTree(Tree tree, Map map)
        {
            if (Vector2.Distance(position, tree.Tile.Position) < 20)
            {
                speed = 0;
                map.ChopTree(tree);
            }
            else
            {
                direction = new Vector2(tree.Tile.Position.X - position.X, tree.Tile.Position.Y - position.Y);
                direction.Normalize();
                speed = 0.5f;
            }
        }
        public void Update(Map map)
        {
            velocity = Vector2.Zero;
            if(map.treesToChop.Count != 0)
            {
                ChopTree(map.treesToChop.ToArray()[0], map);
            }
            velocity = direction * speed;
            position += velocity;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, position, Color.White);
        }
    }
}
