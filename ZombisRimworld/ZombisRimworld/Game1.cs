﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;
using ZombisRimworld.UI;

namespace ZombisRimworld
{
    public class Game1 : Game
    {
        private GraphicsDeviceManager _graphics;
        private SpriteBatch _spriteBatch;
        public static Dictionary<string, Texture2D> textures;
        public static Map map;
        UserInterface UI;
        Human human, human2;

        public Game1()
        {
            _graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            IsMouseVisible = true;
        }

        protected override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            _spriteBatch = new SpriteBatch(GraphicsDevice);
            textures = new Dictionary<string, Texture2D>();
            textures.Add("hoverTile", Content.Load<Texture2D>("selectTile"));
            textures.Add("button", Content.Load<Texture2D>("button"));
            textures.Add("grass", Content.Load<Texture2D>("grass"));
            textures.Add("tree", Content.Load<Texture2D>("tree"));
            textures.Add("human", Content.Load<Texture2D>("human"));
            textures.Add("log", Content.Load<Texture2D>("log"));
            UI = new UserInterface();
            UI.AddUIEntity("ChopTree", new Button(textures["button"], new Vector2(700, 20), null, null));
            UI.AddUIEntity("Build", new Button(textures["button"], new Vector2(700, 50), null, null));
            map = new Map(30, 30);
            human = new Human(textures["human"], new Vector2(200,200));
        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            MouseEvents.Update();
            UI.Update(MouseEvents.LeftClick, MouseEvents.LeftButton, MouseEvents.MousePosition);
            ButtonClickResponse();

            map.Update();
            human.Update(map);

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            _spriteBatch.Begin();

            map.Draw(_spriteBatch);
            UI.Draw(_spriteBatch);
            human.Draw(_spriteBatch);

            _spriteBatch.End();

            base.Draw(gameTime);
        }

        void ButtonClickResponse()
        {
            if (UI.Buttons["ChopTree"].Clicked)
            {
                if (MouseEvents.SelectTree)
                {
                    MouseEvents.SelectTree = false;
                }
                else MouseEvents.SelectTree = true; MouseEvents.Build = false;
            }

            if (UI.Buttons["Build"].Clicked)
            {
                if (MouseEvents.Build)
                {
                    MouseEvents.Build = false;
                }
                else MouseEvents.Build = true; MouseEvents.SelectTree = false;
            }
        }
    }
}
