﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;
using ZombisRimworld.UI;

namespace ZombisRimworld
{
    public class Map
    {
        Tile[,] tiles;
        static int tileSize = 30;
        public List<Tree> treesToChop;
        public List<Tile> toBuild;
        List<Item> items;
        Tile hoveredTile;
        public Map(int sizeX, int sizeY)
        {
            GenerateMap(sizeX, sizeY);
            treesToChop = new List<Tree>();
            items = new List<Item>();
        }

        public void ChopTree(Tree tree)
        {
            treesToChop.Remove(tree);
            items.Add(tree.Tile.destructTileObject());
        }
        void GenerateMap(int sizeX, int sizeY)
        {
            tiles = new Tile[sizeX, sizeY];
            Random rnd = new Random();
            for (int i = 0; i < sizeX; i++)
            {
                for (int j = 0; j < sizeY; j++)
                {
                    if(rnd.Next(0,10) == 1)
                    {
                        tiles[i, j] = new Tile(Game1.textures["grass"], new Vector2(i * tileSize, j * tileSize));
                        tiles[i, j].newTileObject(new Tree(Game1.textures["tree"], tiles[i, j]));
                    }
                    else tiles[i, j] = new Tile(Game1.textures["grass"], new Vector2(i * tileSize, j * tileSize));
                }
            }
        }

        Tile HoveredTile()
        {
            return tiles[MouseEvents.MousePosition.X / tileSize, MouseEvents.MousePosition.Y / tileSize];
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            foreach (Tile tile in tiles)
            {
                tile.Draw(spriteBatch);
            }
            foreach (Item item in items)
            {
                item.Draw(spriteBatch);
            }
        }

        public void Update()
        {
            hoveredTile = HoveredTile();
            foreach (Tile tile in tiles)
            {
                tile.Update(hoveredTile, this);
            }
        }
    }
}
